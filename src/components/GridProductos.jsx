import React, { useState } from 'react';
import '../css/GridProductos.css';
import { Link } from 'react-router-dom';


const GridProductos = ({ products, itemsPerPage }) => {
    const [currentPage, setCurrentPage] = useState(1);
    const [selectedProductoId, setSelectedProductosId] = useState(null);

    const increasedItemsPerPage = 16;
    // const sortedProducts= products.slice().sort((a,b) => a.Nombre.localeCompare(b.nombre));
    const totalPages = Math.ceil(products.length / increasedItemsPerPage);

    const currentProducts = products.slice((currentPage - 1) * increasedItemsPerPage, currentPage * increasedItemsPerPage);

    const handleProductClick = (productId) => {
        setSelectedProductosId(productId)
    }

    return (
        <div>
            <div className="product-grid">
                {currentProducts.map((product) => (
                    <div key={product.ID} className="product-grid__item">
                        <Link to={`/producto/${product.ID}`}>
                            <img src={product.ImgURL} alt={product.name} />
                            <h2 className='product-name'>{product.Nombre}</h2>
                            {/* <p className='product-description'>{product.Descripcion}</p> */}
                        </Link>
                    </div>
                ))}
            </div>
            <div className="pagination">
                <button onClick={() => setCurrentPage(currentPage - 1)} disabled={currentPage === 1}>
                    ←
                </button>

                {Array.from({ length: totalPages }, (_, i) => i + 1).map(page => (
                    <button key={page} onClick={() => setCurrentPage(page)} disabled={page === currentPage}>
                        {page}
                    </button>
                ))}

                <button onClick={() => setCurrentPage(currentPage + 1)} disabled={currentPage === totalPages}>
                    →
                </button>
            </div>

        </div>
    );
};

export default GridProductos;

import React, { useState, useEffect } from 'react';
import getCategorias from '../api/categoria_api';
import getPrincipiosActivos from '../api/principio_activo_api';
import getLineasTerapeuticas from '../api/lineasterapeuticas_api';
import '../css/filtros.css'


const Filtros = ({ setSearchTerm, setCategoria, setLineasTerapeutica, setPrincipioActivo }) => {
    const [categorias, setCategorias] = useState([]);
    const [lineasTerapeuticas, setLineasTerapeuticas] = useState([]);
    const [principiosActivos, setPrincipiosActivos] = useState([]);
    const [loading, setLoading]= useState(true);

    useEffect(() => {
        const fetchData = async () => {
            try{
                const [categoriaData, lineasData, principiosData] = await Promise.all([
                    getCategorias(),
                    getLineasTerapeuticas(),
                    getPrincipiosActivos()

                ]);

                    setCategorias(categoriaData);
                    setLineasTerapeuticas(lineasData);
                    setPrincipiosActivos(principiosData);
                    console.log('categorias',getCategorias);
            }catch (error) {
                console.error('Error al cargar datos', error);
            }finally{
                setLoading(false);
            }
        };
        fetchData();
    },[]);

    if(loading){
        return <div> Cargando...</div>
    }

    const handleCategoriaChange = (e) => {
        setCategoria(e.target.value);
    }

    const handleLineaTerapeuticaChange = (e) => {
        setLineasTerapeutica(e.target.value);
    }

    const handlePrincipioActivoChange = (e) => {
        setPrincipioActivo(e.target.value);
    }

    const  handleSearchTerm = (e) => {
        setSearchTerm(e.target.value);
    }

    return (
        <div className='filtros'>
            <div className="search-container">
                <input
                    type="text"
                    placeholder="Buscar por: nombre comercial"
                    onChange={handleSearchTerm}
                />
                <i className="fa fa-search search-icon"></i>
            </div>
            <select onChange={handleCategoriaChange}>
                <option value="">Seleccionar Categoría</option>
                {categorias && categorias.map((categoria) => (
                    <option key={categoria.ID} value={categoria.ID}>{categoria.Nombre}</option>
                ))}
            </select>

            <select onChange={handleLineaTerapeuticaChange}>
                <option value="">Seleccionar Línea Terapéutica</option>
                {lineasTerapeuticas && lineasTerapeuticas.map((linea) => (
                    <option key={linea.ID} value={linea.ID}>{linea.Nombre}</option>
                ))}
            </select>

            <select onChange={handlePrincipioActivoChange}>
                <option value="">Seleccionar Principio Activo</option>
                {principiosActivos && principiosActivos.map((principio) => (
                    <option key={principio.ID} value={principio.ID}>{principio.Nombre}</option>
                ))}
            </select>
        </div >
    );
};

export default Filtros;

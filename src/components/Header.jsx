import React from 'react';
import '../Header.css'

const Header = () => {
  return (
    <nav class="navbar navbar-expand-lg navbar-light bg-light" id="main-header">
      <div class="container-fluid">
        <a class="navbar-brand logo_container" href="https://bioteng.consultoresinformaticos.com.py/">
          <img src="https://bioteng.consultoresinformaticos.com.py/wp-content/uploads/2023/12/image-removebg-preview-3.png" alt="Bioteng" id="logo" />
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav " id="top-menu">
            <li class="nav-item">
              <a class="nav-link" href="https://bioteng.consultoresinformaticos.com.py/">Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="https://bioteng.consultoresinformaticos.com.py/nosotros/">Nosotros</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="/" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Productos
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">       
                <li><a class="dropdown-item" href="/">Producto 1</a></li>
                <li><a class="dropdown-item" href="/">Producto 2</a></li>
              </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="https://bioteng.consultoresinformaticos.com.py/noticias/">Noticias</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="https://bioteng.consultoresinformaticos.com.py/contacto/">Contacto</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Header;




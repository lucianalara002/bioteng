import React, { useEffect, useState } from 'react'
import { useLocation, useParams } from 'react-router-dom';
import getPresentaciones from '../api/presentaciones';
import getProductos from '../api/producto_api';
import getLineasTerapeuticas from '../api/lineasterapeuticas_api';
import getPrincipiosActivos from '../api/principio_activo_api';
import getCategorias from '../api/categoria_api';
import '../css/detalleProducto.css';
import getPresentacionesid from '../api/presentaciones_id';


const DetalleProducto=() =>  {
  const {id} = useParams()
  const [detallePresentacion, setDetallePresentacion] = useState(null);
  const [producto, setProducto]= useState({});
  const [categoria, setCategoria] = useState('');
  const [lineaTerapeutica, setLineaTerapeutica] = useState('');
  const [principioActivo, setPrincipioActivo] = useState('');

  useEffect(() => {
    const cargarDatos = async () => {
      try{
        const [productoData, presentaciones] = await Promise.all([
          getProductos(id),
          getPresentacionesid(id),
        ]);
          
          setProducto(productoData[(id-1)]);
          setDetallePresentacion(presentaciones);

        const principioActivoData = await getPrincipiosActivos(productoData[id].ID_PrincipioActivo);
        const lineaTerapeuticaData = await getLineasTerapeuticas(productoData[id].ID_LineaTerapeutica);

        if (!principioActivoData){
          console.log('No se encontro el principio activo con el id: ', productoData[id]);
          return
        }
        
        setPrincipioActivo(principioActivoData.Nombre)

        if (!lineaTerapeuticaData){
          console.log('No se encontro el principio activo con el id:', productoData[id])
          return
        }
        setLineaTerapeutica(lineaTerapeuticaData.Nombre)

        //   const categoriaData = await getCategorias(productoData[id].ID_Categoria);

        //   if (!categoriaData){
        //     console.log('No se encontro la categoria con el id:', productoData[id])
        //     return
        //   }
        //     setCategoria(categoriaData.Nombre);
        // }else{
        //     setLineaTerapeutica(lineaTerapeutica.Nombre)
        // }
        
       } catch (error) {
        console.error('Error al cargar los datos: ', error);
      }
    };

      cargarDatos();
    }, [id]);
  
    

  return (
    <div className='detalle-container'>
      <div className='producto-container'>
            <>
              <div className='producto'>
                {producto?.ImgURL ?(
                    <img src={producto.ImgURL} alt={producto.Nombre}/>
                ) : (
                  <p>Imagen no disponiblle</p>
                )}
              </div>
              <div className='detalle-producto'>
                <div className='product-name'>
                  <h2>{producto.Nombre}</h2>
                </div>
                <div className='presentacion'>
                  <h4>Presentación: </h4>
                  <p>°{detallePresentacion ? detallePresentacion.Descripcion : 'No hay información de presentación'}</p>
                </div>
                <div  className='informacion-adicional'>
                  <h4>Información adicional: </h4>
                  <p> ° Linea Terapeutica: {lineaTerapeutica}</p>
                  <p> ° Principio Activo: {principioActivo}</p>
                </div>
              </div>
            </>
        
      </div>
     
    </div>   
  );
};

export default DetalleProducto;
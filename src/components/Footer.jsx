import React from 'react';
import '../css/Footer.css'; 

const Footer = () => {
  return (
      <div className='footer_container'>
        <a class="navbar-brand logo_container" href="https://bioteng.consultoresinformaticos.com.py/">
        <img className='footer_img' src="https://bioteng.consultoresinformaticos.com.py/wp-content/uploads/2023/11/image-removebg-preview-4.png"alt='icono'/>
        </a>
           <div className='informacion'>
                <p className='text'>Atención: Lun.- Vie. 8.00 hs. a 18.00 hs.</p>
          </div>
          <div className='contact'>
              <p className='text'> contact</p>
          </div>
          <div className='extra'>
            <h6>Diseñado por PLOT | Desarrollado por Consultores Informáticos S.A. 2023  </h6>
          </div>
      </div>
  );
};

  
  export default Footer;
  
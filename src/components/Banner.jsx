import React from 'react';
import '../css/Banner.css'; // Asegúrate de que la ruta al archivo CSS es correcta

const Banner = () => {
  return (
      <div className='banner_container'>
          <img className='banner_img' src="https://bioteng.consultoresinformaticos.com.py/wp-content/uploads/2023/12/Productos_.png" />
      </div>
  );
};

  
  export default Banner;
  /*<div className="banner">
        
        </div>*/

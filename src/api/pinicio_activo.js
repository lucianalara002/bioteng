import axios from "axios";
const API_URL = 'http://localhost:5600/api'; 

const getPrincipiosActivos = async (id) => {
    const config ={
      headers: {
        'Content-Type': 'aplication/json',
      },
    };
    const URL = `${API_URL}/principioactivo/${id}`;
      try{
        const response = await axios.get(URL,config);
        const principios_activos = response.data;
        return principios_activos;
      }catch(error){
        if (error.response && error.response.status === 404){
          console.log('El recurso con fue encontrado');
        }else {
          console.log('Error al realizar la solicitud de principio activo');
        }
        throw error;
      }
  }
  
  export default getPrincipiosActivos;
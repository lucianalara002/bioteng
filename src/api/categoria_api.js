import axios from "axios";

const API_URL = 'http://localhost:5600/api'; 

export const getCategorias = async () => {
  const config={
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const URL = `${API_URL}/categorias`;
  try{
  const response = await axios.get(`${URL}`,config);
  const categorias = response.data;

  return categorias;
  }catch(error){
    console.error('Error al realizar la solicitud de categorias', error.message);
  }
};

export default getCategorias;
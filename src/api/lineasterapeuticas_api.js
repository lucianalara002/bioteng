import axios from "axios";
const API_URL = 'http://localhost:5600/api'; 

export const getLineasTerapeuticas = async (id) => {
  const config={
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const URL = `${API_URL}/lineasterapeuticas/${id}`;
  try{
    const response = await axios.get(URL,config);
    const lineasTerapeuticas = response.data;
    return lineasTerapeuticas;
  }catch(error){
    console.error('Error al realizar la solicitud de lineasTerapeuticas', error.message);
    throw error;
  }
};

export default getLineasTerapeuticas;
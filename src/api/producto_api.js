// api.js
import axios from "axios";
const API_URL = 'http://localhost:5600/api'; 

export const getProductos = async (id) => {
  const config={
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const URL = `${API_URL}/productos`;
  try{
  const response = await axios.get(`${URL}`,config);
  const products = response.data;
  return products;
  }catch(error){
    console.error('Error al realizar la solicitud de productos', error.message);
  }
};

/*

export async function getProductos() {
    const response = await fetch(API_URL + "/productos");
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    }
    return await response.json();
}*/
export default getProductos;

/*export async function getSomeData() {
  const response = await fetch(`${API_URL}/some-endpoint`);
  if (!response.ok) {
    throw new Error(`HTTP error! status: ${response.status}`);
  }
  return await response.json();
}*/

/*export async function postSomeData(data) {
  const response = await fetch(`${API_URL}/some-endpoint`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
  if (!response.ok) {
    throw new Error(`HTTP error! status: ${response.status}`);
  }
  return await response.json();
}*/

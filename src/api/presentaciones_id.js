import axios from "axios";
const API_URL = 'http://localhost:5600/api'; 

 const getPresentacionesid = async (productoId) => {
  const config={
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const URL = `${API_URL}/presentaciones/${productoId}`;
  try{
    const response = await axios.get(`${URL}`,config);
    const presentaciones = response.data
    return presentaciones;
  }catch (error) {
    console.error('error al obtener presentaciones', error);
  }
};
export default getPresentacionesid;
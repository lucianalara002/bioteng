import './App.css';
import Header from './components/Header'
import Banner from './components/Banner'
import Footer from './components/Footer';
import GridProductos from './components/GridProductos';
import getProductos from './api/producto_api';
import React, { useEffect, useState, useCallback } from 'react';
import Filtros from './components/Filtros';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import DetalleProducto from './components/DetalleProducto';
import getPresentaciones from './api/presentaciones';
import getPresentacionesid from './api/presentaciones_id';

function App() {
  // ...
  const [productos, setProductos] = useState([]);
  const [categoria, setCategoria] = useState([]);
  const [lineaTerapeutica, setLineaTerapeutica] = useState([]);
  const [principioActivo, setPrincipioActivo] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [productosFiltrados, setProductosFiltrados] = useState([]);
  const [presentaciones, setPresentaciones] = useState([]);

  const cargarProductos = useCallback(async () => {
    const response = await getProductos();
    setProductos(response);
    setProductosFiltrados(response);
  }, []);

  // const cargarPresentaciones=useCallback(async () => {
  //   try{
  //     const response = await getPresentacionesid();
  //     setPresentaciones(response);
  //     console.log("response",response);
  //   }catch (error) {
  //     console.error('Error al cargar', error)
  //   }
  // }, []);

  useEffect(() => {
    cargarProductos();
  }, [cargarProductos]);


  useEffect(() => {
    buscarProductos(searchTerm);
  }, [searchTerm]);

  // useEffect (() => {
  //   cargarPresentaciones();
  // }, [cargarPresentaciones])

  function buscarProductos(term) {
    const res = productos.filter((item) => item.Nombre.toLowerCase().includes(term.toLowerCase()) || term === "");
    setProductosFiltrados(res);
  };

  const productosFiltradosFinal = productosFiltrados.filter(producto => {
    return (
      (categoria.length === 0 || categoria.includes(producto.ID_Categoria)) &&
      (lineaTerapeutica.length === 0 || lineaTerapeutica.includes(producto.ID_LineaTerapeutica)) &&
      (principioActivo.length === 0 || principioActivo.includes(producto.ID_PrincipioActivo))
    );
  });


  return (
    <Router>
    <Header />
    <Routes>
      <Route
        path="/"
        element={
          <>
            <Banner />
            <Filtros
              setSearchTerm={setSearchTerm}
              setCategoria={setCategoria}
              setLineasTerapeutica={setLineaTerapeutica}
              setPrincipioActivo={setPrincipioActivo}
            />
            <GridProductos products={productosFiltradosFinal} itemsPerPage={8} />
          </>
        }
      />
      <Route path="/producto/:id" element={<DetalleProducto />} />
    </Routes>
    <React.Fragment>
      {/* <Footer /> */}
    </React.Fragment>
 </Router>
 
);
}
export default App;

